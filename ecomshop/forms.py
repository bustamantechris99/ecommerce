from django.forms import ModelForm
from django import forms
from .models import OrderItem, ShippingAddress

class CartForm(ModelForm):
    class Meta:
        model = OrderItem
        fields = ['quantity']

class LoginForm(forms.Form):
    username = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs={'placeholder':'Username', 'size':'60'}))
    password = forms.CharField(max_length=200, required=True, widget=forms.PasswordInput(attrs={'placeholder':'Password', 'size':'60'}))



class RegisterForm(forms.Form):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder':'username', 'size':'60'}))
    password = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={'placeholder':'password', 'size':'60'}))
    password_confirmation = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={'placeholder':'password-confirmation', 'size':'60'}))
    email = forms.EmailField(max_length=200, widget=forms.PasswordInput(attrs={'placeholder':'email', 'size':'60'}))
    first_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder':'first name', 'size':'60'}))
    last_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder':'last name', 'size':'60'}))


class AccountForm(ModelForm):
    class Meta:
        model = ShippingAddress
        fields = ['first_name', 'last_name', 'address', 'city', 'state', 'zipcode']



class UpdateAccountForm(ModelForm):
    class Meta:
        model = ShippingAddress
        fields = ['first_name', 'last_name', 'address', 'city', 'state', 'zipcode']