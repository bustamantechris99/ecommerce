from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login, logout
from .models import Product, Order, OrderItem, Customer, ShippingAddress
from .forms import CartForm, LoginForm, RegisterForm, AccountForm, UpdateAccountForm
from django.contrib.auth.models import User
import random

def shop(request):
    products = Product.objects.all()
    if request.user.is_authenticated:
        loggedinCart = Order.objects.filter(customer=request.user.customer).first()
    else:
        loggedinCart=None

    print(loggedinCart)
    context = {
        'products': products,
        'cart': loggedinCart
      }

    return render(request, 'ecomshop/shop.html', context)


def detail(request, id):
    product = get_object_or_404(Product, id=id)
    context = {
      'product':product
    }
    return render(request, 'ecomshop/detail.html', context)

def cart(request):
    if request.user.is_authenticated:
        loggedin = request.user.customer
        loggedinCart = Order.objects.filter(customer=request.user.id).first()
        if Order.objects.filter(customer=loggedin).exists():
            order = Order.objects.get(customer=loggedin, complete=False)
        else:
            order = Order.objects.create(customer=loggedin, complete=False)
        # order, created = Order.objects.get_or_create(customer = loggedin, complete=False)
        items = order.items.all()
    else:
      items = []
      order = {'get_cart_total':0, 'get_cart_items':0}
      loggedinCart=None
    context = {
      'order': order,
      'items': items,
      'cart': loggedinCart
    }
    return render(request, 'ecomshop/cart.html', context)

def checkoutpage(request):
    if request.user.is_authenticated:
        loggedin = request.user.customer
        order = Order.objects.filter(customer=loggedin).first()
        items = order.items.all()
        try:
            accountinfo = ShippingAddress.objects.get(customer=request.user.customer)
        except:
            accountinfo=''
        context = {
            'order': order,
            'items': items,
            'accountinfo': accountinfo
        }

        return render(request, 'ecomshop/checkout.html', context)




def add_to_cart(request, id):
    product = get_object_or_404(Product, id=id)
    if request.method == 'POST':
        form = CartForm(request.POST)
        if form.is_valid():
        #   order = form.cleaned_data['order']
            order = Order.objects.get(customer=request.user.customer)
        #  or ---> order = Order.objects.get(customer__user=request.user)
            quant = form.cleaned_data['quantity']
            try:
                item = OrderItem.objects.get(product=product, order=order, item_num=id)
                OrderItem.objects.filter(item_num=id).update(quantity = item.quantity + quant)
                return redirect('cart')
            except:
                OrderItem.objects.create(product = product, order=order, quantity=quant, item_num=id)
                return redirect('cart')
    else:
        form = CartForm()
    context = {
        'form': form,
        'product': product
    }
    return render(request, 'ecomshop/add_to_cart.html',context)

def delete_cart_item(request, id):
    item = get_object_or_404(OrderItem, pk=id)
    context = {
        'item': item
    }
    if request.method == 'GET':
        return render(request, 'ecomshop/delete-cart-item.html', context)
    elif request.method == 'POST':
        item.delete()
        return redirect('cart')

def LogIn(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('shop')
    else:
        form = LoginForm()
    context = {
      'form': form
    }
    return render(request, 'ecomshop/login.html', context)

def LogOut(request):
    logout(request)
    return redirect('shop')

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        numint=''
        for num in range(11):
            rand = random.randint(0,9)
            numint+=str(rand)
        transaction_num=int(numint)
        print(transaction_num)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name'].lower()
            last_name = form.cleaned_data['last_name']
            if password == password_confirmation:
                user = User.objects.create_user(username=username, email=email, password=password, first_name=first_name)
                login(request, user)
                if user.is_authenticated:
                    Customer.objects.create(user=user, name=first_name, email=email)
                    Order.objects.create(customer=request.user.customer, complete=False, trasaction=transaction_num)
                return redirect('shop')
            else:
                form.add_error('password',"Sorry passwords don't match!")
    else:
        form = RegisterForm()
    context = {
        'form':form}
    return render(request, 'ecomshop/register.html', context)


def accountinfo(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            info = form.save(False)
            info.customer = Customer.objects.get(user=request.user)
            info.order = Order.objects.get(customer=request.user.customer)
            info.save()
            return redirect('account')
    else:
        form = AccountForm()
    try:
        accountinfo = ShippingAddress.objects.get(customer=request.user.customer)
    except:
        accountinfo=None
    context = {
        'form': form,
        'accountinfo': accountinfo
    }
    return render(request, 'ecomshop/account.html', context)

# def editaccountinfo(request, id):
#     account = ShippingAddress.objects.get(customer=request.user.customer, order=Order.objects.get(customer=request.user.customer))
#     if request.method == 'POST':
#         form = UpdateAccountForm(request.POST)
#         if form.is_valid():
#             first = form.cleaned_data['first_name']
#             second = form.cleaned_data['second_name']
#             address = form.cleaned_data['address']
#             city = form.cleaned_data['city']
#             state = form.cleaned_data['state']
#             zipcode = form.cleaned_data['zipcode']
#             customer=Customer.objects.get(user=request.user)
#             order= Order.objects.get(customer=request.user.customer)
#             ShippingAddress.objects.filter(id=id).update(customer=customer, order=order, first_name=first, second_name=second, address=address, city=city, state=state, zipcode=zipcode)
#         return redirect('account')
#     else:
#         form = UpdateAccountForm()
#     context = {
#         'form': form
#     }
#     return render(request, 'ecomshop/updateaccount.html', context)

def editaccountinfo(request):
    account = ShippingAddress.objects.get(customer=request.user.customer, order=Order.objects.get(customer=request.user.customer))
    form = UpdateAccountForm(request.POST, instance=account)
    if form.is_valid():
            new = form.save(False)
            new.customer = Customer.objects.get(user=request.user)
            new.order = Order.objects.get(customer=request.user.customer)
            new.save()
            return redirect('account')
    else:
        form = AccountForm(instance=account)
    context = {
        'form': form
    }
    return render(request, 'ecomshop/updateaccount.html', context)
