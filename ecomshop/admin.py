from django.contrib import admin
from .models import Customer, Order, OrderItem, ShippingAddress, Product

admin.site.register(Customer)
admin.site.register(Order)
@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
        list_display=(
          'product',
          'order',
          'id'


        )

@admin.register(ShippingAddress)
class ShippinAddressAdmin(admin.ModelAdmin):
    list_display=(
          'id', )

admin.site.register(Product)
