from django.urls import path
from .views import shop, detail, cart, add_to_cart, delete_cart_item, LogIn, LogOut, accountinfo, register, checkoutpage, editaccountinfo

urlpatterns = [
    path('', shop, name='shop'),
    path('img/<int:id>/', detail, name='image'),
    path('cart/', cart, name='cart'),
    path('addtocart/<int:id>/', add_to_cart, name='add'),
    path('deleteitem/<int:id>/', delete_cart_item, name='delete-item'),
    path('login/', LogIn, name='login'),
    path('logout/', LogOut, name='logout'),
    path('account/', accountinfo, name='account'),
    path('register/', register, name='register'),
    path('checkout/', checkoutpage, name='checkout'),
    path('updateaccount/', editaccountinfo, name='updateaccount')
]
