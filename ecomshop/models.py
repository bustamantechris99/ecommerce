from django.db import models
from django.conf import settings

class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='customer', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)

    def __str__(self):
        return str(self.user)

class Product(models.Model):
    name = models.CharField(max_length=200, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    image = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name

class Order(models.Model):
    customer = models.ForeignKey(Customer, related_name='order', on_delete=models.CASCADE, null=True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    complete = models.BooleanField(default=False, null=True)
    trasaction = models.CharField(max_length=200)

    def __str__(self):
        return str(self.customer)

    @property
    def cart_total(self):
        items_total = []
        for item in self.items.all():
            items_total.append(item.item_total)
        return sum(items_total)
    @property
    def get_cart_items_count(self):
        count = 0
        for item in self.items.all():
            count += item.quantity
        return count


class OrderItem(models.Model):
    product = models.ForeignKey(Product, related_name='product', on_delete=models.SET_NULL, blank=True, null=True)
    order = models.ForeignKey(Order, related_name='items', on_delete=models.SET_NULL, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    item_num = models.IntegerField(default=0)

    def __str__(self):
        return str(self.product)

    @property
    def item_total(self):
        return self.product.price * self.quantity


class ShippingAddress(models.Model):
    customer = models.ForeignKey(Customer, related_name='shipping_address', on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, related_name='shipping_address', null=True)
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    state = models.CharField(max_length=200, null=True)
    zipcode = models.CharField(max_length=200, null=True)

    def __str__(self):
        return str(self.customer)
